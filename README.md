# egs00

xml → tlv

Library of choice for XML parsing - [expat](https://github.com/libexpat/libexpat/tree/R_2_4_8)

Known issues:

* No endianess differentiation to write
* Error handing in the converter is not used to the full power
* wchar_t handing from the XML is probably broken in some conditions, but not entirely broken at least
* Unit testing not implemented; Initial plans to implement unit testing with [unity](http://www.throwtheswitch.org/unity) had been broken by time consuming circumstances.
