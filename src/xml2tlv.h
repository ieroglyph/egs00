#ifndef XML2TLV_H
#define XML2TLV_H 1

typedef enum
{
    Xml2Tlv_NoError,
    Xml2Tlv_XmlError,
    Xml2Tlv_OutError,
    Xml2Tlv_DataError,
} Xml2TlvError;

struct Xml2TlvStruct;
typedef struct Xml2TlvStruct *Xml2Tlv;

// Initialize converter
Xml2Tlv Xml2Tlv_Create();

// Delete converter, free memory
void Xml2Tlv_Free(Xml2Tlv converter);

// Set params: 
// ofile: output file for binary data
// xmlParser: xpat xml parser
void Xml2Tlv_Configure(Xml2Tlv converter, void* ofile, void* xmlParser);

// Get error description
void Xml2Tlv_GetError(Xml2Tlv converter);

#endif//XML2TLV_H