#include <stdio.h>
#include <expat.h>
#include "xml2tlv.h"
#include "defines.h"

int main(int argc, int* argv[])
{
    char buf[10000];
    int done = 0;

    if (argc < 3) {
        printf("Not enought arguments. Expected call format: $egs00 input.xml output.bin");
        exit(-1);
    }
    
    FILE *xmlFile;
    xmlFile = fopen((const char*)argv[1],"r"); 
    if (!xmlFile) {
        printf("Error opening xml file: %s", argv[1]);
        exit(-1);
    }

    FILE* tlvFile;
    tlvFile = fopen((const char*)argv[2], "wb"); 
    if (!tlvFile)  {
        fclose(xmlFile);
        printf("Error opening output file: %s", argv[2]);
        exit(-1);
    }

    XML_Parser parser = XML_ParserCreate(NULL);
    if (! parser) {
        fclose(xmlFile);
        fclose(tlvFile);
        fprintf(stderr, "Couldn't allocate memory for parser\n");
        exit(-1);
    }

    Xml2Tlv converter = Xml2Tlv_Create();
    if (! converter) {
        fclose(xmlFile);
        fclose(tlvFile);
        XML_ParserFree(parser);
        fprintf(stderr, "Couldn't allocate memory for converter\n");
        exit(-1);
    }

    Xml2Tlv_Configure(converter, tlvFile, parser);

    do {
        fread(buf,sizeof(buf),1,xmlFile); 
        if (XML_Parse(parser, buf, sizeof(buf), done) == XML_STATUS_ERROR) {
            fprintf(stderr, "%" XML_FMT_STR " at line %" XML_FMT_INT_MOD "u\n",
                    XML_ErrorString(XML_GetErrorCode(parser)),
                    XML_GetCurrentLineNumber(parser));
            fclose(tlvFile);
            fclose(xmlFile);    
            Xml2Tlv_Free(converter);
            XML_ParserFree(parser);
            return 1;
        }
    } while (! done);

    fclose(tlvFile);
    fclose(xmlFile);   
    Xml2Tlv_Free(converter);
    XML_ParserFree(parser);
    return 0;
}
