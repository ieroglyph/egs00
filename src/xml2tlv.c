#include <expat.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "xml2tlv.h" // self
#include "defines.h"

// Tags in XML to search for
const XML_Char TagNameText[] = "text";
const XML_Char TagNameNumber[] = "number";
const XML_Char TagNameStart[] = "START";

// Tags for TLV
typedef enum 
{
    TAG_NUMBER = 0x11,
    TAG_TEXT = 0x22,
} TlvTags;

// State of converting process
typedef enum
{
    XmlState_Unknown = 0, // nothing special
    XmlState_StartOpened = 1, // initial tag opened
    XmlState_StringOpened = 2, // expecting string data
    XmlState_NumberOpened = 4, // expecting number data
    XmlState_ErrorState = 16, // expecting number data, // expecting number data
    XmlState_ValueOpened = XmlState_StringOpened | XmlState_NumberOpened,
} XmlState;

// Main struct to incapsulate the Converter data 
struct Xml2TlvStruct
{
    FILE* ofile;
    XML_Parser parser;
    XmlState currentXmlState;
    Xml2TlvError errorCode;
    XML_Char buf[1024]; // also max string length
    XML_Char* bufp;
};
typedef struct Xml2TlvStruct* Converter;

// Parse numerical value from XML and write to file
static void
Xml2Tlv_AddNumber(Converter cnv) {
    if (!cnv) 
        return;
    if (!cnv->ofile) {
        cnv->errorCode = Xml2Tlv_OutError;
        return;    
    }
    int value = atoi(cnv->buf);
    int tag = TAG_NUMBER;
    int size = sizeof(int);
    // TODO: add LittleEndian/BigEndian support
    fwrite(&tag, sizeof(int), 1, cnv->ofile);
    fwrite(&size, sizeof(int), 1, cnv->ofile);
    fwrite(&value, sizeof(int), 1, cnv->ofile);
}

// Parse text value from XML and write to file
static void
Xml2Tlv_AddText(Converter cnv) {
    if (!cnv) 
        return;
    if (!cnv->ofile) {
        cnv->errorCode = Xml2Tlv_OutError;
        return;    
    }
    // TODO: add LittleEndian/BigEndian support
    int tag = TAG_TEXT;
    int size = strlen(cnv->buf);
    fwrite(&tag, sizeof(int), 1, cnv->ofile);
    fwrite(&size, sizeof(int), 1, cnv->ofile);
    fwrite((const char*)cnv->buf, size, 1, cnv->ofile);
}

// start reading: find out what that is and set state
static void XMLCALL
startElement(void *userData, const XML_Char *name, const XML_Char **) {
  Converter cnv = (Converter)userData;
  if ((cnv->currentXmlState & XmlState_ValueOpened) != 0) {
      // ERROR: unexpected tag started
        fprintf(stderr, "Unexpected tag %" XML_FMT_STR " found\n", name);
      return;
  }
  if (strcmp(name,TagNameText) == 0) {
      cnv->currentXmlState |= XmlState_StringOpened;
  } else if (strcmp(name, TagNameNumber) == 0) {
      cnv->currentXmlState |= XmlState_NumberOpened;
  } else if (strcmp(name, TagNameStart) == 0) {
      cnv->currentXmlState |= XmlState_StartOpened;
  } // else ignore unknown tag
}

// stop reading: reset buffer pointer, call for writing the data to ofile
static void XMLCALL
endElement(void *userData, const XML_Char *) {
    Converter cnv = (Converter)userData;
    if ((cnv->currentXmlState & XmlState_ErrorState) == XmlState_ErrorState) {
        // nothing good to expect here, let's go away
        fprintf(stderr, "No point closing elements in error state\n");
        cnv->errorCode = Xml2Tlv_XmlError;
        return;
    } 
    if ((cnv->currentXmlState & XmlState_StringOpened) == XmlState_StringOpened)
    {
        Xml2Tlv_AddText(cnv);
    } 
    if ((cnv->currentXmlState & XmlState_NumberOpened) == XmlState_NumberOpened)
    {
        Xml2Tlv_AddNumber(cnv);
    }
    cnv->currentXmlState &= XmlState_StartOpened;
    cnv->bufp = cnv->buf;
    memset(cnv->buf,0,sizeof(cnv->buf));
}

// Collect the value buffer, this is quite a pickle
static void XMLCALL
dataElement(void *userData, const XML_Char *data, int len) {
    Converter cnv = (Converter)userData;
    if ((cnv->currentXmlState & XmlState_ErrorState) == XmlState_ErrorState) {
        // nothing good to expect here, let's go away
        fprintf(stderr, "Cannot read data in error state\n");
        cnv->errorCode = Xml2Tlv_XmlError;
        return;
    }
    if ((cnv->currentXmlState & XmlState_ValueOpened) == 0) {
        // tag not started, skipping tabs or whatever is between data tags in the XML
        return;
    }
    // Buffer overflow? Something smart to do here according to the buisness logic; 
    // just ignore the rest for now.
    if ((cnv->bufp - cnv->buf) > sizeof(cnv->buf)) {
        return;
    };
    memcpy(cnv->bufp, data, len*sizeof(XML_Char));
    cnv->bufp += len*sizeof(XML_Char);
}

// Allocate some memory, set some defaults
Converter Xml2Tlv_Create()
{
    Converter converter;
    converter = (Converter)malloc(sizeof(struct Xml2TlvStruct));
    if (!converter)
        return converter;
    converter->currentXmlState = XmlState_Unknown;
    converter->errorCode = Xml2Tlv_NoError;
    memset(converter->buf, 0, sizeof(converter->buf));
    converter->bufp = converter->buf;
    return converter;
}

// "Connect" relations b/w parser and converter, set some data falues
void Xml2Tlv_Configure(Converter converter, void* ofile, void* xmlParser)
{
    if (!converter || !ofile || !xmlParser)
        return;

    converter->ofile = (FILE*)ofile;
    converter->parser = (XML_Parser)xmlParser;
    
    XML_SetUserData(xmlParser, converter);
    XML_SetElementHandler(xmlParser, startElement, endElement);
    XML_SetCharacterDataHandler(xmlParser, dataElement);
}

// Free the memory
void Xml2Tlv_Free(Converter converter)
{
    free(converter);
}

// Get verbose error
void Xml2Tlv_GetError(Xml2Tlv converter)
{    
    if (!converter)
        return;

    switch(converter->errorCode) {
        case Xml2Tlv_NoError:
            fprintf(stderr, "No errors occured");
            break;
        case Xml2Tlv_XmlError:
            fprintf(stderr, "Xml schema error");
            break;
        case Xml2Tlv_OutError:
            fprintf(stderr, "Output file error");
            break;
        case Xml2Tlv_DataError:
            fprintf(stderr, "Data conversion error");
            break;
    }
}